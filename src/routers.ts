import HealthCheckRoute from './healthcheck/health.check.route';
import HomeRoute from './home/home.route';
import RegistrationRoute from './registration/registration.route';
import UserRoute from './user/user.route';

const routersCollection = [
  new HomeRoute(),
  new HealthCheckRoute(),
  new UserRoute(),
  new RegistrationRoute()
];
export default routersCollection;
