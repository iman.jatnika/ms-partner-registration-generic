import { AppError } from '../errors/AppError';
import { NextFunction, Response, Request } from 'express';
const userMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const body = req.body;
    if (body) {
      next();
    } else {
      next(new AppError(400, 'partner Id is empty'));
    }
  } catch (error) {}
};
export default userMiddleware;
