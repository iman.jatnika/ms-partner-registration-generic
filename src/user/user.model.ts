import mongoose, { Schema, Document, Model } from 'mongoose';
import { IUserModel } from './user.interface';

export type UserDocument = IUserModel & Document;

export const collectionName = 'users';

const UserSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true
    }
  },
  {
    timestamps: true
  }
);

export const UserModel: Model<UserDocument> = mongoose.model(
  collectionName,
  UserSchema
);
