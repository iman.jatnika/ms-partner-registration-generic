import { NewEntity } from '../common/type';
import { IUserModel } from './user.interface';
import userRepository from './user.repository';
import redisUtil from '../common/redis.util';

const create = async (user: NewEntity<IUserModel>): Promise<IUserModel> => {
  const userData = await redisUtil.getKey(user.email);
  if (userData) {
    return JSON.parse(userData);
  }
  const result = await userRepository.create(user);
  if (result) {
    redisUtil.setKey(result.email, JSON.stringify(result));
  }
  return result;
};

const userService = {
  create
};
export default userService;
