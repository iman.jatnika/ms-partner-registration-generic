import { NewEntity } from '../common/type';
import { IUserModel } from './user.interface';
import { UserDocument, UserModel } from './user.model';

const convertUserDocumentToObject = (document: UserDocument) =>
  document.toObject({ getters: true }) as IUserModel;

const create = async (user: NewEntity<IUserModel>): Promise<IUserModel> => {
  const document = await UserModel.create(user);
  return convertUserDocumentToObject(document);
};

const update = async (data: IUserModel): Promise<IUserModel | null> => {
  const result = await Promise.resolve(
    UserModel.findOneAndUpdate(
      {
        email: data.email
      },
      data,
      {
        new: true
      }
    )
  );
  return result && convertUserDocumentToObject(result);
};

const findUserById = async (userId: string): Promise<IUserModel | null> => {
  const user = await Promise.resolve(UserModel.findOne({ _id: userId }));

  return user && convertUserDocumentToObject(user);
};

const deleteById = async (userId: string): Promise<IUserModel | null> => {
  const user = await Promise.resolve(
    UserModel.findByIdAndDelete({ _id: userId })
  );

  return user && convertUserDocumentToObject(user);
};
const userRepository = {
  create,
  update,
  findUserById,
  deleteById
};
export default userRepository;
