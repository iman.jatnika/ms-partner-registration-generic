import { NextFunction, Request, Response } from 'express';
import { validationResult } from 'express-validator';
import { IUserModel } from './user.interface';
import userRepository from './user.repository';
import userService from './user.service';

class UserController {
  public create = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }
      const userData: IUserModel = req.body;
      const result = await userService.create(userData);
      res.status(201).json(result);
    } catch (error) {
      next(error);
    }
  };

  public getUserId = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      const userId: string = req.params.id;
      const result = await userRepository.findUserById(userId);

      res.status(200).json({ result });
    } catch (error) {
      next(error);
    }
  };

  public getDeleteById = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      const userId: string = req.params.id;
      const result = await userRepository.deleteById(userId);

      res.status(200).json({ result });
    } catch (error) {
      next(error);
    }
  };

  public getUpdate = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      const userData: IUserModel = req.body;
      const result = await userRepository.update(userData);

      res.status(200).json({ result });
    } catch (error) {
      next(error);
    }
  };
}

export default UserController;
