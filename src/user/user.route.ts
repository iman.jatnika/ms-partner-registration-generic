import { Router } from 'express';
import { Routes } from '../common/routes.interface';
import UserController from './user.controller';
import { createUserValidation } from './user.validator';

class UserRoute implements Routes {
  public path = '/users';
  public router = Router();
  public userController = new UserController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(
      `${this.path}`,
      createUserValidation,
      this.userController.create
    );
    this.router.get(`${this.path}/:id`, this.userController.getUserId);
    this.router.put(`${this.path}`, this.userController.getUpdate);
    this.router.delete(`${this.path}/:id`, this.userController.getDeleteById);
  }
}

export default UserRoute;
