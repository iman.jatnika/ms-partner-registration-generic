import { check, header } from 'express-validator';

export const createUserValidation = [
  check('name').isLength({ min: 3 }),
  check('email').isEmail(),
  check('password').isStrongPassword(),
  header('partner-id').isLength({ min: 3 })
];
