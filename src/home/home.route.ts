import { Router } from 'express';
import { Routes } from '../common/routes.interface';
import HomeController from './home.controller';

class HomeRoute implements Routes {
  public path = '/';
  public router = Router();
  public homeController = new HomeController();
  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.homeController.home);
  }
}

export default HomeRoute;
