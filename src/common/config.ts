const ServerConfig = {
  PORT: 3000
};

const DbConfig = {
  HOST: 'localhost',
  PORT: '27017',
  DATABASE_NAME: 'partner-generic-db'
};

const RedisConfig = {
  HOST: '127.0.0.1',
  PORT: 6379
};

export { ServerConfig, DbConfig, RedisConfig };
