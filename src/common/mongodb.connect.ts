import { DbConfig } from './config';

export const dbConnection = {
  url: `mongodb://${DbConfig.HOST}:${DbConfig.PORT}/${DbConfig.DATABASE_NAME}`,
  options: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  }
};
