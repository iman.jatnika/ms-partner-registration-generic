import Redis from 'ioredis';
import { RedisConfig } from './config';

const redis = new Redis({ port: RedisConfig.PORT, host: RedisConfig.HOST });
const getKey = (key: string): Promise<string | null> => {
  return redis.get(key);
};
const setKey = (key: string, value: string): Promise<string | null> => {
  return redis.set(key, value);
};
const deleteKey = (key: string): Promise<number | null> => {
  return redis.del(key);
};
const redisUtil = { getKey, setKey, deleteKey };
export default redisUtil;
