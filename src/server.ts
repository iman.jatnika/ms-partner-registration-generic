import App from './app';
import routersCollection from './routers';

const app = new App(routersCollection);

app.listen();
