export interface IRegistrationModel {
  name: string;
  email: string;
  idNumber: string;
  phoneNumber: string;
  partnerId: string;
}
