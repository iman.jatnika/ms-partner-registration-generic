import { NextFunction, Request, Response } from 'express';
import { validationResult } from 'express-validator';
import registrationService from './registration.service';
class RegistrationController {
  public create = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }
      const partnerId = req.headers['partner-id'];
      const payload = req.body;
      const result = await registrationService.create({...payload, partnerId});
      res.status(201).json(result);
    } catch (error) {
      next(error);
    }
  };
}
export default RegistrationController;