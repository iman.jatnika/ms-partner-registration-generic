import { Router } from 'express';
import { Routes } from '../common/routes.interface';
import RegistrationController from './registration.controller';
import { registrationValidation } from './registration.validator';

class RegistrationRoute implements Routes {
  public path = '/registration';
  public router = Router();
  public registrationController = new RegistrationController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(
      `${this.path}/register`,
      registrationValidation,
      this.registrationController.create
    );
  }
}

export default RegistrationRoute;
