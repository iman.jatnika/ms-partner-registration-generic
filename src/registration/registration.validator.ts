import { check, header } from 'express-validator';

export const registrationValidation = [
  check('name').isLength({ min: 3 }),
  check('email').isEmail(),
  check('idNumber').isLength({ max: 16 }),
  check('phoneNumber').isLength({ max: 14 }),
  header('partner-id').isLength({ min: 1 })
];
