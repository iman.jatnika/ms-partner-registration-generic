import mongoose, { Schema, Document, Model } from 'mongoose';
import { IRegistrationModel } from './registration.interface';

export type RegistrationDocument = IRegistrationModel & Document;

export const collectionName = 'registrations';

const RegistrationSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true
    },
    idNumber: {
      type: String,
      required: true
    },
    phoneNumber: {
      type: String,
      required: true
    },
    partnerId: {
      type: String,
      required: true
    }
  },
  {
    timestamps: true
  }
);

export const RegistrationModel: Model<RegistrationDocument> = mongoose.model(
  collectionName,
  RegistrationSchema
);
