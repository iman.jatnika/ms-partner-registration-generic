import { NewEntity } from '../common/type';
import { IRegistrationModel } from './registration.interface';
import { RegistrationDocument, RegistrationModel } from './registration.model';

const convertRegistrationDocumentToObject = (document: RegistrationDocument) =>
  document.toObject({ getters: true }) as IRegistrationModel;

const create = async (
  registration: NewEntity<IRegistrationModel>
): Promise<IRegistrationModel> => {
  const document = await RegistrationModel.create(registration);
  return convertRegistrationDocumentToObject(document);
};
const registrationRepository = {
  create
};
export default registrationRepository;
