import { NewEntity } from '../common/type';
import redisUtil from '../common/redis.util';
import { IRegistrationModel } from './registration.interface';
import registrationRepository from './registration.repository';

const create = async (registration: NewEntity<IRegistrationModel>): Promise<IRegistrationModel> => {
  const registrationData = await redisUtil.getKey(registration.email);
  if (registrationData) {
    return JSON.parse(registrationData);
  }
  const result = await registrationRepository.create(registration);
  if (result) {
    redisUtil.setKey(result.email, JSON.stringify(result));
  }
  return result;
};

const registrationService = {
  create
};
export default registrationService;
