import express from 'express';
import { connect } from 'mongoose';
import { ServerConfig } from './common/config';
import { dbConnection } from './common/mongodb.connect';
import { Routes } from './common/routes.interface';

class App {
  public app: express.Application;
  public port: string | number;

  constructor(routes: Routes[]) {
    this.app = express();
    this.port = ServerConfig.PORT;
    this.connectToDatabase();
    this.initializeMiddlewares();
    this.initializeRoutes(routes);
  }

  public listen() {
    this.app.listen(this.port, () => {
      console.log(` ---  listening on the port ${this.port} is started ---`);
    });
  }

  public getServer() {
    return this.app;
  }

  private connectToDatabase() {
    connect(dbConnection.url, dbConnection.options, () => {
      console.log('connect to db');
    });
  }

  private initializeMiddlewares() {
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
  }
  private initializeRoutes(routes: Routes[]) {
    console.log('---- available path ---');
    routes.forEach((route) => {
      console.log(route.path);
      this.app.use('/', route.router);
    });
    console.log('-----------------------');
  }
}
export default App;
