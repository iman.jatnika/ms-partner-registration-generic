import { Router } from 'express';
import { Routes } from '../common/routes.interface';
import HealthCheckController from './health.check.controller';
import healthCheckMiddleware from './health.check.middleware';

class HealthCheckRoute implements Routes {
  public path = '/ping';
  public router = Router();
  public healtCheckController = new HealthCheckController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(
      `${this.path}`,
      healthCheckMiddleware,
      this.healtCheckController.getPing
    );
  }
}

export default HealthCheckRoute;
