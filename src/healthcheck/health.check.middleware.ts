import { AppError } from '../errors/AppError';
import { NextFunction, Response, Request } from 'express';
const healthCheckMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const partnerId = req.header('partner-id');
    if (partnerId) {
      next();
    } else {
      next(new AppError(400, 'partner Id is empty'));
    }
  } catch (error) {}
};

export default healthCheckMiddleware;
