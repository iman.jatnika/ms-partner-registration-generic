import { NextFunction, Request, Response } from 'express';

class HealthCheckController {
  public getPing = async (
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> => {
    try {
      const headerValue = req.headers['partner-id'];
      console.log(headerValue);
      res.status(200).send('Pong');
    } catch (error) {
      next(error);
    }
  };
}
export default HealthCheckController;
